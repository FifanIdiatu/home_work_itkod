class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Привет, меня зовут {self.name} и мне {self.age} лет.")


class Dog:
    def __init__(self, name, breed):
        self.name = name
        self.breed = breed

    def bark(self):
        print(f"Гав-гав! Я {self.name}, порода {self.breed}.")


person1 = Person("Иван", 25)
dog1 = Dog("Рекс", "овчарка")
person1.say_hello()
dog1.bark()
